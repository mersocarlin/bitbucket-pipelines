const request = require('supertest');
const express = require('express');
const expect = require('chai').expect;

const application = require('../src');

describe('ping api', () => {
  let app;

  before(function () {
    app = application.application();
  });

  describe('/ping', () => {
    it('should return status = 0 for /api/ping', done => {
      request(app)
        .get('/api/ping')
        .expect(200)
        .end((err, res) => {
          if (err) {
            done(err);
            return;
          }

          expect(res.body).to.not.be.null;
          expect(res.body).to.have.property('status');
          expect(res.body.status).to.equal(0);
          done();
        });
    });

    it('should throw not found', done => {
      request(app)
        .get('/api/tadada')
        .expect(404)
        .end(done);
    });
  });
});
