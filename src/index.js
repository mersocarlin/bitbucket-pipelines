const express = require('express');

module.exports = {
  application: function () {
    const app = express();

    app.get('/api/ping', function (req, res) {
      res.send({ status: 0 });
    });

    return app;
  },

  start: function () {
    this
      .application()
      .listen(3000, function () {
        console.log('[bitbucket-pipelines] listening on port 3000');
      });
  }
};
