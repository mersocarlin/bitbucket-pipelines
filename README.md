# bitbucket-pipelines

This is a sample project used to describe how to use bitbucket-pipelines in your repository.

It is also part of my tutorial *Continuous Delivery with Bitbucket Pipelines* posted on [Medium](https://medium.com/@mersocarlin/continuous-delivery-with-bitbucket-pipelines-f15b829fda1b).

## License

MIT